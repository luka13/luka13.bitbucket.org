
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
 
 var podatki = [
  // ime     priimek starost temp visina teza
   ["Jože","Podbevšek","1990-03-05", 37, 170, 120,"id1"],
   ["Manca","Černobil","1970-01-07", 40, 168, 50,"id2"],
   ["Primož","Starokopitnik","1950-01-9", 36, 175, 70,"id3"]
   ];
//console.log(podatki[0][0]);
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
$(document).ready(function() {
   /* if($('#enka').selected) {    
        $('#seznam').change(function() {
            $('#kreirajIme').val(podatki[0][0]);
            $('#kreirajPriimek').val(podatki[0][1]);
            $('#kreirajStarost').val(podatki[0][2]);
        });
    }
    else if($('#dvojka').selected) {    
        $('#seznam').change(function() {
            $('#kreirajIme').val(podatki[1][0]);
            $('#kreirajPriimek').val(podatki[1][1]);
            $('#kreirajStarost').val(podatki[1][2]);
        });
    }
     else if($('#trojka').selected) {    
        $('#seznam').change(function() {
            $('#kreirajIme').val(podatki[2][0]);
            $('#kreirajPriimek').val(podatki[2][1]);
            $('#kreirajStarost').val(podatki[2][2]);
        });
    }*/
      $('#seznam').change(function() {
    
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajStarost").val(podatki[2]);
  });
  document.getElementById('preberiEhr').addEventListener('keyup',function() {
      var ehr = document.getElementById('preberiEhr').value;
      if (ehr == podatki[0][6]) {
            $("#preberiTežo").val(podatki[0][5]);
            $("#preberiVišino").val(podatki[0][4]);  
      }
      else if (ehr == podatki[1][6]) {
            $("#preberiTežo").val(podatki[1][5]);
            $("#preberiVišino").val(podatki[1][4]); 
      }
      else if (ehr == podatki[2][6]) {
            $("#preberiTežo").val(podatki[2][5]);
            $("#preberiVišino").val(podatki[2][4]); 
      }
      
  });
});




function generirajPodatke(stPacienta) {
  

     $.ajaxSetup({
        headers: {
            "Authorization": getAuthorization()
        }
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            podatki[stPacienta - 1][6] = ehrId;
            $("#header").html("EHR: " + ehrId);
    
            //ustvari novo osebo
            
            var partyData = {
                ime: podatki[stPacienta - 1][0],
                priimek: podatki[stPacienta - 1][1],
                partyAdditionalInfo: [
                    {
            "starost":podatki[stPacienta - 1][2],
          
                   
            "temperatura":podatki[stPacienta - 1][3],
          
                   
            "višina": podatki[stPacienta - 1][4],
          
                   
            "teža": podatki[stPacienta - 1][5],
            
            "ehrId": ehrId
          }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        $("#result").html("Created: " + party.meta.href);
                    }
                }
            });
            
        }
        
    });
/*$("#dodan").html("<span class='obvestilo1'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");*/
    if(stPacienta == 3){
    document.getElementById("seznam").disabled = false;
    }

  return null;
}

function ustvariEHR() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var starost = $("#kreirajStarost").val();
    
    
    
    
    if (ime == podatki[0][0] && priimek == podatki[0][1] && starost == podatki[0][2]) {
        $("#dodan").html("<span class='obvestilo1'>Oseba ki ste jo izbrali ima EHR '" +
                podatki[0][6] + "'.</span>");
    }
    else if (ime == podatki[1][0] && priimek == podatki[1][1] && starost == podatki[1][2]) {
        $("#dodan").html("<span class='obvestilo1'>Oseba ki ste jo izbrali ima EHR '" +
                podatki[1][6] + "'.</span>");
    }
    else if (ime == podatki[2][0] && priimek == podatki[2][1] && starost == podatki[2][2]) {
        $("#dodan").html("<span class='obvestilo1'>Oseba ki ste jo izbrali ima EHR '" +
                podatki[2][6] + "'.</span>");
    }
	else if (!ime || !priimek || !starost || ime.trim().length == 0 ||
      priimek.trim().length == 0 || starost.trim().length == 0) {
		$("#dodan").html("<span class='obvestilo2'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          
          additionalInfo: {
            "ehrId": ehrId,
            "starost": starost
            }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#dodan").html("<span class='obvestilo1'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              //$("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#dodano").html("<span class='obvestilo2 label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}
var sprememba;
		function createConfig(pointStyle) {
			return {
				type: 'line',
				data: {
					labels: ['Vaš ITM', 'Povprečni ITM', 'Najvišji normalni ITM', 'Najnižji normalni ITM'],
					datasets: [{
						label: 'ITM',
						backgroundColor: "white",
						borderColor: "red",
						data: [0, 25, 25, 18.5],
						fill: false,
						pointRadius: 10,
						pointHoverRadius: 15,
						showLine: false // no line shown
					}]
				},
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Vaš index telesne mase v primerji s povprečnim, najvišjim normalnim in najnižjim normalnim ITM'
					},
					legend: {
						display: false
					},
					elements: {
						point: {
							pointStyle: pointStyle
						}
					}
				}
				
			};
		}
		window.onload = function() {
			var container = document.querySelector('#grafek');
			[
				'circle',
				
			].forEach(function(pointStyle) {
				var div = document.createElement('div');
				div.classList.add('chart-container');
				var canvas = document.getElementById('graf');
				div.appendChild(canvas);
				container.appendChild(div);
				var ctx = canvas.getContext('2d');
				var config = createConfig(pointStyle);
				sprememba = new Chart(ctx, config);
			});
		};
	
   function addData(vrednost) {
       sprememba.data.datasets[0].data[0] = vrednost;
       sprememba.update();
       
       
   }
   function izrisiGraf() {
       var ime = $("#preberiEhr").val();
       var teža = $("#preberiTežo").val();
       var višina = $("#preberiVišino").val();
       var BMI = teža/((višina*višina)/10000);
    
    
    if (ime.length != 36 || teža.length == 0 || višina.length == 0) {
        document.getElementById('uspesno').hidden = true;
        document.getElementById('napaka').hidden = false;
        
    }
    else {
        addData(BMI);
        if(BMI < 18.5) {
            //$("#prenizek").style.visibility = 'visible';
            document.getElementById('prenizek').hidden = false;
            document.getElementById('previsok').hidden = true;
            document.getElementById('ok').hidden = true;
        }
        else if(BMI >= 18.5 && BMI <= 25) {
            document.getElementById('ok').hidden = false;
            document.getElementById('prenizek').hidden = true;
            document.getElementById('previsok').hidden = true;
        }
        else if(BMI > 25) {
            document.getElementById('previsok').hidden = false;
            document.getElementById('prenizek').hidden = true;
            document.getElementById('ok').hidden = true;
            
        }
        document.getElementById('uspesno').hidden = false;
        document.getElementById('napaka').hidden = true;
    }
       
       
       
       
   };
   
    
    
    


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
